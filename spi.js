
var SPI = require('snow/spi'); // 라이브러리를 호출합니다.
var spi = new SPI.spi(); // 객체를 생성합니다.

spi.mode(3); // spi 모드를 설정합니다.
spi.chipSelect(0); // cs시그널 타입을 설정합니다.

spi.open(); // 장치를 시작합니다.

var buff = new Buffer([0x56, 0x78, 0x88]); // 전송할 버퍼를 선언합니다.
var buff2 = new Buffer(3); // 수신할 버퍼를 선언합니다.

spi.transfer(buff, buff2); // 송수신 기능을 수행합니다.
spi.close(); // 장치를 종료합니다.

