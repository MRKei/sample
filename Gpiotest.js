console.log("================ GPIO TEST ================");

var GPIO = require('snow/gpio.node');
 var gpio = new GPIO.gpio();
 gpio.open();
 console.log(" set D0 mode: Out");
 gpio.pinMode(0, 1); // D0 out
 console.log(" set D0 value: 1");
 gpio.digitalWrite(0,1);
 console.log(" set D0 read" + gpio.digitalRead(0));
 console.log(" set D0 value : 0");
 gpio.digitalWrite(0, 0);
 console.log(" set D0 read" + gpio.digitalRead(0));