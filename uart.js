var UART = require('snow/uart'); // 라이브러리를 호출합니다.
var uart = new UART.uart(); 

var ch = 1; 

uart.open(ch); // uart 1번 채널을 시작합니다.
uart.settings(ch, 9600, 8, 0, 1);

uart.puts(ch, 'Input char : 5'); // uart 1번 채널로 “input char :5” 라는 문자열을 전달합니다.

setInterval(function () { // 반복함수를 설정합니다.
  console.log(uart.getch(ch)); // 1번 채널로 송신된 1byte 문자를 콘솔 화면으로 출력합니다.
}, 1000);
  
uart.close(ch);   
