module.exports = Joint;

function Joint(
	tag
	)

{
  this.tag = tag;

  console.log(
    "[WJANG][" + this.tag + "] Joint Created"
    );
}

Joint.prototype.init = function init(
	servo,
	minAngle,
	maxAngle,
	direction,
	stepOfAngle,
	defaultAngle
	)
{
	this.servo = servo;
	this.minAngle = minAngle;
	this.maxAngle = maxAngle;
	this.direction = direction;
	this.stepOfAngle = stepOfAngle;
  this.defaultAngle = defaultAngle;
	this.angle = this.defaultAngle;//this.minAngle + ( ( this.maxAngle - this.minAngle ) / 2 );

  console.log(
    "[WJANG][" + this.tag + "] Joint Init"
    );

	servo.move( this.defaultAngle, 1 );
}

Joint.prototype.close = function close()
{
	console.log("[WJANG][" + this.tag + "] Joint Close" );
}

Joint.prototype.rotate = function rotate(
	direction
	)
{
	if( direction == 1 )
	{
		this.angle += (this.stepOfAngle * this.direction);
	}
	else
	{
		this.angle -= (this.stepOfAngle * this.direction);
	}

	
  if( this.angle > this.maxAngle)
	{
		this.angle = this.maxAngle;
		return;
	}
	else if( this.angle < this.minAngle)
	{
		this.angle = this.minAngle;
		return;
	}

	console.log("[WJANG][" + this.tag + "] Angle: " + this.angle );

	this.servo.move( this.angle, 1 );
}
