var I2C = require('snow/i2c'); 
var i2c = new I2C.i2c(); 

var rbuf = new Buffer(12);
var wbuf = new Buffer(2);

i2c.open("/dev/i2c-0");
  
wbuf[0] = 0x6B; // reg addr
wbuf[1] = 0x00; // data
i2c.write(0x68, wbuf);

  
rbuf[0]= i2c.read(0x68, 0x3B);
  

i2c.close();
