
console.log ("===========================");
console.log (" read Device Time ");
console.log ("===========================");

var RTC = require('snow/rtc');
var rtc = new RTC.rtc(); 

rtc.open();

console.log("DEVICE TIME : " + rtc.getTime());

  
console.log ("===========================");
console.log (" Set Device Time ");
console.log ("===========================");


rtc.setTime(2015, 1, 1, 12, 0, 0);  // 2015/1/1 12:00:00

console.log("DEVICE TIME : " + rtc.getTime());

rtc.close(); 


