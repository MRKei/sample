
var ADC = require('snow/adc');
var adc = new ADC.adc(); 

adc.open(); 

adc.attach(); // adc 장치를 attach 합니다.


console.log ("===========================");
console.log (" ADC Test "); 
console.log ("===========================");


console.log(" read adc0 :" + adc.read(0, 1000)); // 0번 채널의 값을 읽어 콘솔 화면으로 값을 출력합니다.

adc.detach(); // adc채널 사용이 끝난 경우, adc를 detach합니다.

adc.close(); // 장치를 종료합니다.
