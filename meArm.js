var SERVO = require("Servo");             
var Servo_X = new SERVO( "Servo_X" );
var Servo_Y_L = new SERVO( "Servo_Y_L" );
var Servo_Y_R = new SERVO( "Servo_Y_R" );
var Servo_Gripper = new SERVO( "Servo_Gripper" );

var ADC = require('snow/adc');
var adc = new ADC.adc(); 
adc.open(); 
adc.attach();

var ADCREADER = require("ADCReader"); 
var ADCReader_X = new ADCREADER("ADCReader_X");
var ADCReader_Y_L = new ADCREADER("ADCReader_Y_L");
var ADCReader_Y_R = new ADCREADER("ADCReader_Y_R");
var ADCReader_Gripper = new ADCREADER("ADCReader_Gripper");

var JOINT = require("Joint");    
var Joint_X = new JOINT( "Joint_X" );
var Joint_Y_L = new JOINT( "Joint_Y_L" );
var Joint_Y_R = new JOINT( "Joint_Y_R" );
var Joint_Gripper = new JOINT( "Joint_Gripper" );

var minAngle = 0;
var maxAngle = 180;
var defaultReadTimeout = 1000;
var defaultStepOfAngle = 5;

process.on('SIGINT', function() {
  
  clearInterval(t);
  
  Servo_X.close();
  Servo_Y_L.close();
  Servo_Y_R.close();
  Servo_Gripper.close();
  
  ADCReader_X.close();
  ADCReader_Y_L.close();
  ADCReader_Y_R.close();
  ADCReader_Gripper.close();
  
  Joint_X.close();
  Joint_Y_L.close();
  Joint_Y_R.close();
  Joint_Gripper.close();

  adc.detach();
  adc.close();

  process.exit(1);
  console.log('Got SIGINT.');

});

Servo_X.init(
  0,        // pin
  50,       // freq
  0.5,      // min pulse interval (ms)
  2.0,      // max pulse interval (ms)
  180       // maxAngle
  );     

Servo_Y_L.init(
  2,        // pin
  50,       // freq
  0.5,      // min pulse interval (ms)
  2.0,      // max pulse interval (ms)
  180       // maxAngle
  );    

Servo_Y_R.init(
  4,        // pin
  50,       // freq
  0.5,      // min pulse interval (ms)
  2.0,      // max pulse interval (ms)
  180       // maxAngle
  );    

Servo_Gripper.init(
  6,        // pin
  50,       // freq
  0.5,      // min pulse interval (ms)
  2.0,      // max pulse interval (ms)
  180       // maxAngle
  );    

ADCReader_X.init(
  adc,  // global adc instance
  0,    // adc number
  defaultReadTimeout,   // read interval
  4095  // max value
  );

ADCReader_Y_L.init(
  adc,  // global adc instance
  1,    // adc number
  defaultReadTimeout,   // read interval
  4095  // max value
  );
 
ADCReader_Y_R.init(
  adc,  // global adc instance
  2,    // adc number
  defaultReadTimeout,   // read interval
  4095  // max value
  );
 
ADCReader_Gripper.init(
  adc,  // global adc instance
  3,    // adc number
  defaultReadTimeout,   // read interval
  4095  // max value
  );
 
Joint_X.init(
  Servo_X,
  0,    // minAngle
  180,  // maxAngle
  1,    // Direction
  defaultStepOfAngle,     // step of angle
  90
  );

Joint_Y_L.init(
  Servo_Y_L,
  0,    // minAngle
  180,  // maxAngle
  -1,    // Direction
  defaultStepOfAngle,     // step of angle
  90
  );

Joint_Y_R.init(
  Servo_Y_R,
  0,    // minAngle
  180,  // maxAngle
  1,    // Direction
  defaultStepOfAngle,     // step of angle
  90
  );

Joint_Gripper.init(
  Servo_Gripper,
  120,  // minAngle
  180,  // maxAngle
  1,   // Direction
  2,    // step of angle
  180
  );

var X = 0;
var Y_L = 1;
var Y_R = 2;
var GRIPPER = 3;
var MAX_JOINT_CNT = 4;

var mode = X;

var JointArray = [];
var ADCReaderArray = [];

JointArray[X] 		  = Joint_X;
JointArray[Y_L] 	  = Joint_Y_L;
JointArray[Y_R] 	  = Joint_Y_R;
JointArray[GRIPPER] = Joint_Gripper;

ADCReaderArray[X] 		  = ADCReader_X;
ADCReaderArray[Y_L] 		= ADCReader_Y_L;
ADCReaderArray[Y_R] 		= ADCReader_Y_R;
ADCReaderArray[GRIPPER]	= ADCReader_Gripper;

var t = setInterval(function() {
  
  CurrentAdc = ADCReaderArray[mode];
  CurrentJoint = JointArray[mode];
  
  adcValue = CurrentAdc.read();
  if( adcValue != -1 ) CurrentJoint.rotate( adcValue );
  
  if( mode == GRIPPER ) mode = X;
  else mode ++;
  
}, 10);

console.log("FINISH");
