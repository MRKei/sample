module.exports = ADCReader;

function ADCReader(
	tag
	)

{
  this.tag = tag;

  console.log(
    "[WJANG][" + this.tag + "] ADCReader Created"
    );
}

ADCReader.prototype.init = function init(
	adc,
	pin,
	readTime,
	maxValue
	)
{
  this.adc = adc;
  this.pin = pin;
  this.readTime = readTime;
  this.maxValue = maxValue;
  this.quota = this.maxValue / 8;
  this.adcValue = -1;
  
  console.log(
    "[WJANG][" + this.tag + "] ADCReader Init"
    );
}

ADCReader.prototype.close = function close()
{
	console.log("[WJANG][" + this.tag + "] ADCReader Close" );
}

ADCReader.prototype.read = function read()
{
	this.adcValue = this.adc.read(this.pin, this.readTime)/this.quota;
  this.adcValue = Math.floor(this.adcValue);
 
  if( this.adcValue == 0 ) 
  {
    this.adcValue = 0;
  }
  else if( this.adcValue == 7 ) 
  {
    this.adcValue = 1;
  }
  else 
  {
    //console.log("[WJANG][" + this.tag + "] read value is out of range: " + this.adcValue );
    this.adcValue = -1;
  }
  
  if( this.adcValue != -1 )
  {
    console.log("[WJANG][" + this.tag + "] read: " + this.adcValue );
  }
	return this.adcValue;

}
