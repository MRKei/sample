
var PWM = require('snow/pwm');
var pwm = new PWM.pwm();   

var freq = 2000;  // 2000Hz 
var duty = 50;    // dute 50% 

console.log ("===========================");
console.log (" PWM Test "); 
console.log ("==========================="); 

pwm.open(); // 장치를 시작합니다.
pwm.enable(); 
pwm.setFreq(freq, duty); // 2000Hz, duty 50% / PWM 신호의 주파수와 Duty를 설정합니다.

//pwm.disable(); // PWM 출력을 멈춥니다.

//pwm.close(); 

