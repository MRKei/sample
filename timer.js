var TIMER = require('snow/timer'); // 라이브러리를 호출합니다.
var timer = new TIMER.timer(); // 객체를 생성합니다.

timer.open(); // 장치를 시작합니다.

timer.setCallback(function (i) {
  console.log('cb :' + i);
}); // 콜백함수를 설정합니다.

var index = timer.set(); 

timer.start(index, 2000000); // 2초 간격으로 타이머 발생하도록 설정합니다.

// ....

timer.delete(index); // index 의 타이머를 해제합니다.
timer.close(); // 장치를 종료합니다.
