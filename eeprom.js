  
console.log ("===========================");
console.log (" EEPROM read/write Test ");
console.log ("===========================");

var e2prom = require('snow/eeprom');
var eeprom = new e2prom.eeprom(); 

eeprom.open('/dev/mtd2');

var wdata = new Buffer("test");
var rbuf = new Buffer(36); 


console.log (" EEPROM Block Erase");
console.log ("===========================");
eeprom.eraseBlock(0); 


console.log (" EEPROM write : "+ wdata.toString("utf8", 0, 4));
console.log ("===========================");
eeprom.write(0, wdata, 4); 

console.log (" EEPROM read");
console.log ("===========================");
eeprom.read(0,rbuf,4);
console.log(rbuf.toString("utf8",0,4));


console.log (" EEPROM Erase");
console.log ("===========================");
eeprom.erase(); 

eeprom.close(); 
