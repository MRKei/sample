module.exports = Servo;

var gTimerModule;
var gTimerInstance;

gTimerModule = require("snow/timer");
gtimerInstance = new gTimerModule.timer();
gtimerInstance.open();

function Servo(
    tag
  ) 
{
  this.tag = tag;

  console.log(
    "[WJANG][" + this.tag + "] Servo Created"
    );
}
 
getMinInterval = function(a, b)
{
    if (b === 0)
        return a;

    return getMinInterval(b, a % b);
};

Servo.prototype.init = function init(
    pin,
    freq,
    minPulse,
    maxPulse,
    maxAngle
  ) 
{
  this.pin = pin;
  this.freq = freq;
  this.minPulse = minPulse;
  this.maxPulse = maxPulse;
  this.maxAngle = maxAngle;
  this.msOfStep = 1000 / this.freq;
  this.currentAngle = 0;

  this.GPIOModule = require("snow/gpio");
  this.gpio = new this.GPIOModule.gpio();

  this.gpio.open();
  this.gpio.pinMode(this.pin, 1);

  console.log(
    "[WJANG][" + this.tag + "] Servo Init, "
    + "-TAG: " + this.tag + ", "
    + "-pin: " + this.pin + " th, "
    + "-freq: " + this.freq + " Hz, "
    + "-msOfStep: " + this.msOfStep + " ms, "
    + "-minPulse: " + this.minPulse + " ms, "
    + "-maxPulse: " + this.maxPulse + " ms, "
    + "-maxAngle: " + this.maxAngle + "'"
    );
}

Servo.prototype.move = function move(
    angle,
    count
  ) 
{
  ///*
  if( angle < this.minAngle )
  {
    console.log("[WJANG[" + this.tag + "] Servo Invalid Angle #1: " + angle);
    angle = this.minAngle;
    return;
  }
  if( angle > this.maxAngle )
  {
    console.log("[WJANG][" + this.tag + "] Servo Invalid Angle #2: " + angle);
    angle = this.maxAngle;
    return;
  }
  //*/

  this.currentAngle = angle;
  
  this.stepOfPulse =  ( this.maxPulse - this.minPulse ) / this.maxAngle;
  this.currentPulse =  ( this.stepOfPulse * angle ) + this.minPulse;  
  this.currentPulse.toFixed(3);
  this.currentPulse =  Math.floor(this.currentPulse * 1000)/1000;
 
  this.quotaOfPulse = this.currentPulse / this.minInterval;
  this.quotaOfSec =  this.msOfStep / this.minInterval;
  
  var timerCount = 0;
  var gpio = this.gpio;
  var pin = this.pin;
  var tag = this.tag;
  
  console.log(
      "[WJANG][" + this.tag + "] "
      + "Angle: " + angle 
      + ", currentPulse: " + (this.currentPulse * 1000000) + " ns"   
      );  

  gtimerInstance.setCallback( 
    
    function(i)
    {
      if( timerCount < count )
      {
        gpio.digitalWrite(pin, 0);
      }
      else
      {
        gtimerInstance.delete(i);
      }
      timerCount++;
    }
  );
  console.log(
      "[WJANG][" + this.tag + "] "
      + "Timer: " + this.currentPulse * 1000 
      + " ns"   
      ); 
      
  this.gpio.digitalWrite(this.pin, 1); 
  this.timerIndex = gtimerInstance.set();
  gtimerInstance.start( this.timerIndex, this.currentPulse * 1000 );

}

Servo.prototype.close = function close() 
{
  this.gpio.close();
  gtimerInstance.close();
  console.log("[WJANG][" + this.tag + "] Servo Close");
}
